const path = require('path');
const os = require('os');
const fs = require('fs');
const { app, BrowserWindow, Menu, ipcMain, shell } = require('electron');
const resizeImg = require('resize-img');


const isDev = process.env.NODE_ENV !== 'production';
const isMac = process.platform === 'darwin'

let mainWindow;

// function to create the desktop app main window
function createMainWindow() {
    mainWindow = new BrowserWindow({
        title: 'Image Resizer',
        // +500 in dev env for devtools
        width: isDev ? 1000 : 500,
        height: 600,
        webPreferences: {
            contextIsolation: true,
            nodeIntegration: true,
            preload: path.join(__dirname, 'preload.js'),
        },
    });

    // Open devtools if in dev env
    if(isDev) {
        mainWindow.webContents.openDevTools();
    }

    mainWindow.loadFile(path.join(__dirname, './renderer/index.html'));
}

// function to create about window
function createAboutWindow() {
    const aboutWindow = new BrowserWindow({
        title: 'About Image Resizer',
        // +500 in dev env for devtools
        width: 300,
        height: 300
    });

    aboutWindow.loadFile(path.join(__dirname, './renderer/about.html'));
}

// create a promise on what to do when app is ready
app.whenReady().then(() => {
    createMainWindow();

    //implement menu
    const mainMenu = Menu.buildFromTemplate(menu);
    Menu.setApplicationMenu(mainMenu);

    // remove mainWindow from memory on close
    mainWindow.on('closed', () => (mainWindow = null));

    app.on('activate', () => {
        if (BrowserWindow.getAllWindows.length === 0) {
            createMainWindow();
        }
    })
});

// menu template
const menu = [
    ...(isMac ? [{
        label: app.name,
        submenu : [
            {
                label: 'About',
                click: createAboutWindow,
            }
        ]
    }] : []),
    {
        role: 'fileMenu',
    },
    ...(!isMac ? [{
        label: 'Help',
        submenu : [
            {
                label: 'About',
                click: createAboutWindow,
            }
        ]
    }] : []),
];

// respond to ipcRenderer resize
ipcMain.on('image:resize', (e, options) => {
    options.dest = path.join(os.homedir(), 'imageResizer')
    resizeImage(options);
})

// resize image
async function resizeImage({imgPath, width, height, dest}) {
    try {
        const newPath = await resizeImg(fs.readFileSync(imgPath), {
            width: +width,
            height: +height
        });
        // create filename
        const filename = path.basename(imgPath); 

        // create dest folder if not exists
        if (!fs.existsSync(dest)) {
            fs.mkdirSync(dest);
        }

        // write file to dest
        fs.writeFileSync(path.join(dest, filename), newPath); 

        // send success message 
        mainWindow.webContents.send('image:done');

        // open dest folder
        shell.openPath(dest);
    } catch (error) {
        console.log(error);
    }
}

// for mac users, applications do not quit when windows are closed
app.on('window-all-closed', () => {
    if(!isMac) {
        app.quit()
    }
})