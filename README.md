# Desktop App Electron


# Welcome to Image Resizer!

You want some help on how to use this **Image Resizer** ? Don't worry, read this and everything will go smoothly. [English explanation](#markdown-header-english-explanations)

Besoin d'aide pour utiliser mon **Image Resizer** ? Pas de soucis, il suffit de lire ceci et tout ira bien. [Explications francaises](#markdown-header-explications-francaises)


## English explanations

Click on the "select an image to resize" button,
Select your image,
Chose a new size,
Save it. 
And it's done !



## Explications françaises

Clique sur le bouton "select an image to resize",
Sélectionne ton image,
Choisi une nouvelle taille,
Enregistre la,
Et c'est bon !
